import { IsNotEmpty, Length, MinLength, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 64)
  name: string;

  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  password: string;

  @IsNotEmpty()
  gender: string;
}
